Version {
	var pathToGit, document, randSeed, testMode;
	var <git, <date, <fileName, pathToFile;

	*new { |pathToGit, document, randSeed, testMode = false|
		^super.newCopyArgs(pathToGit, document, randSeed, testMode).init
	}

	init {
		randSeed = randSeed ?? { 0.rrand(10000) };
		thisThread.randSeed_(randSeed);
		pathToFile = document.path;
		fileName = pathToFile.asPathName.fileNameWithoutExtension ++ "_" ++ randSeed ++ "_";
		testMode.not.if({
			document.isEdited.if({
				Error("Please save the document before running!").throw
			});
			date = Date.getDate.stamp;
			git = Git.new(pathToGit);
			git.git(["add", this.escapeAll(pathToFile)]);
			git.git(["commit", "-m", fileName ++ date]);
			fileName = fileName ++ git.sha[0..6] ++ "_";
		});
	}

	escapeAll { |string|
		[$ , $#, $), $(, $&, $$, $@, $!, $%, $^, $*].do{arg char;
			string = string.escapeChar(char)
		};
		^string
	}
}



